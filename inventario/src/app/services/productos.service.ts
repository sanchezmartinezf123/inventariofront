import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http:HttpClient) { }
  
  //Obtener productos
  getProductos(){
   return this.http.get('http://localhost/inventario/inventarioback/inventarioback/relations?select=producto,descripcion,precio,cantidad,categoria,marca&rel=productos,categorias,marcas&type=producto,categoria,marca');
  }

}
