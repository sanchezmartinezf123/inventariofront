import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActualizarComponent } from './actualizar/actualizar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { EditarComponent } from './editar/editar.component';
import { EliminarComponent } from './eliminar/eliminar.component';
import { ListarComponent } from './listar/listar.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {path:'agregar', component: AgregarComponent},
      {path:'actualizar', component: ActualizarComponent},
      {path:'editar', component: EditarComponent},
      {path:'eliminar', component: EliminarComponent},
      {path:'listar', component: ListarComponent},
      {path: '**', redirectTo:'agregar'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
