import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { AgregarComponent } from './agregar/agregar.component';
import { Routes } from '@angular/router';
import { ActualizarComponent } from './actualizar/actualizar.component';
import { EditarComponent } from './editar/editar.component';
import { EliminarComponent } from './eliminar/eliminar.component';
import { ListarComponent } from './listar/listar.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {path:'productos', component: AgregarComponent}
    ]
  }
];

@NgModule({
  declarations: [AgregarComponent, ActualizarComponent, EditarComponent, EliminarComponent, ListarComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule
  ]
})
export class ProductosModule { }
