import { Component, OnInit } from '@angular/core';
import { productos } from 'src/app/interfaces/productos.interface';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styles: [
  ]
})
export class ListarComponent implements OnInit {

  products:productos[]=[];

  constructor(private service: ProductosService) { }

  ngOnInit(): void {
    this.service.getProductos().
    subscribe(
      (productos : any)=>{
        this.products = productos.res;
        console.log(this.products);
      }
    )
  }

}
