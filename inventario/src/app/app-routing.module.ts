import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventarioModule } from './inventario/inventario.module';
import { LoginComponent } from './login/login.component';
import { ProductosModule } from './productos/productos.module';
import { SidemenuComponent } from './shared/sidemenu/sidemenu.component';
import { UsersComponent } from './usuarios/users/users.component';
import { UsuariosModule } from './usuarios/usuarios.module';

/* const routes: Routes = [
  { path:'',pathMatch:'full',redirectTo:'login'},
  { path:'login',component:LoginComponent},
  { path:'registro-usuario',component:UsersComponent},
  { path:'home',component:SidemenuComponent},
  { path:'usuarios',component:UsuariosModule},
  { path:'productos/agregar',component:InventarioModule}
]; */

const routes: Routes = [
  { path:'',pathMatch:'full',redirectTo:'login'},
  { path:'login',component:LoginComponent},
  { path:'registro-usuario',component:UsersComponent},
  { path:'home',component:SidemenuComponent},
  { path:'usuarios',component:UsuariosModule},
  { path:'productos/editar',component:ProductosModule},
  {
      path:'productos',
      loadChildren:() => import('./productos/productos.module').then(m=>m.ProductosModule)
  },
  {
    path:'inventario',
    loadChildren:() => import('./inventario/inventario.module').then(m=>m.InventarioModule)
},
{
  path:'usuarios',
  loadChildren:() => import('./usuarios/usuarios.module').then(m=>m.UsuariosModule)
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
