import { Component, OnInit } from '@angular/core';
interface General{
  title: String, 
  url: any
}

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styles: [
  ]
})
export class SidemenuComponent implements OnInit {

  menuProductos:General[]=[
    {
      title:'Listar Productos',
      url:'/productos/listar'
    },
    {
      title:'Nuevo Producto',
      url:'/productos/agregar'
    }
  ]
  menuUsuarios: General[]=[
    {
      title:'Listar usuarios',
      url:'/usuarios/users'
    }
  ]
  menuInventario: General[]=[
    {
      title:'Listar Inventario',
      url:'/usuarios/users'
    }
  ]
  menuCategorias: General[]=[
    {
      title:'Listar Categorias',
      url:'/usuarios/users'
    }
  ]
  
  constructor() { }

  ngOnInit(): void {
  }

}
