import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { UsersComponent } from '../usuarios/users/users.component';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { ProductosModule } from '../productos/productos.module';

const routes: Routes = [
  { path:'',pathMatch:'full',redirectTo:'login'},
  { path:'login',component:LoginComponent},
  { path:'registro-usuario',component:UsersComponent},
  { path:'home',component:SidemenuComponent},
  { path:'usuarios',component:UsuariosModule},
  { path:'productos/agregar',component:ProductosModule}
];

@NgModule({
  declarations: [SidemenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[SidemenuComponent]
})
export class SharedModule { }
