export class productos {
    constructor (
        public producto:string,
        public descripcion:string,
        public precio: number,
        public cantidad:number,
        public categoria:string,
        public marca:string,
        public id_producto?: number
    ){}
}