import { AgregarCategoriasComponent } from './agregar-categorias/agregar-categorias.component';
import { ListarCategoriasComponent } from './listar-categorias/listar-categorias.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path:'',
    children:[

      {path:'listarcategoria', component: ListarCategoriasComponent},
      {path:'agregarcategoria', component: AgregarCategoriasComponent},


    ]
  }
];;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriasRoutingModule { }
