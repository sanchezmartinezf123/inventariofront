import { AgregarCategoriasComponent } from './agregar-categorias/agregar-categorias.component';
import { Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { ListarCategoriasComponent } from './listar-categorias/listar-categorias.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {path:'categorias', component: AgregarCategoriasComponent}
    ]
  }
];
@NgModule({
  declarations: [
    ListarCategoriasComponent,AgregarCategoriasComponent
  ],
  imports: [
    CommonModule,
    CategoriasRoutingModule
  ]
})
export class CategoriasModule { }
